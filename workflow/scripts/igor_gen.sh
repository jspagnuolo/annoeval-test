#!/bin/bash

reps=$1
nTCR=$2
out_alpha=$3
out_beta=$4

for ((i=1;i<=$reps;i++));
do 
  immuno-probs -threads 10 -out-name $i -set-wd $out_alpha generate -model human-t-alpha -n-gen $nTCR
  immuno-probs -threads 10 -out-name $i -set-wd $out_beta generate -model human-t-beta -n-gen $nTCR
done