#!/bin/bash

### This works!
mkdir ./data/trust4
mkdir ./data/trust4/alpha
mkdir ./data/trust4/beta
docker run --rm -v /$(pwd):/tmp -w /tmp trust4 ../trust4/run-trust4 -t 8 -f ../trust4/hg38_bcrtcr.fa --ref ../trust4/human_IMGT+C.fa -u ./data/tcrsim/$sim_meth/alpha/$fastQin -o ./data/trust4/alpha/${fastQin/.fq/}
docker run --rm -v /$(pwd):/tmp -w /tmp trust4 ../trust4/run-trust4 -t 8 -f ../trust4/hg38_bcrtcr.fa --ref ../trust4/human_IMGT+C.fa -u ./data/tcrsim/$sim_meth/beta/$fastQin -o ./data/trust4/beta/${fastQin/.fq/}

#donor_1_1_airr.tsv extract data from this file.... its in AIRR format so should be easy to condense.

### This works!
mkdir ./data/migmap
mkdir ./data/migmap/alpha
mkdir ./data/migmap/beta
docker run --rm  -v /$(pwd):/tmp -w /tmp migmap java -Xmx16G -jar ../migmap-1.0.3/migmap-1.0.3.jar -p 16 --data-dir ../migmap-1.0.3/data --all-alleles -S human -R TRA ./data/tcrsim/$sim_meth/alpha/$fastQin ./data/migmap/alpha/${fastQin/.fq/.out} 
docker run --rm  -v /$(pwd):/tmp -w /tmp migmap java -Xmx16G -jar ../migmap-1.0.3/migmap-1.0.3.jar -p 16 --data-dir ../migmap-1.0.3/data --all-alleles -S human -R TRB ./data/tcrsim/$sim_meth/beta/$fastQin ./data/migmap/beta/${fastQin/.fq/.out} 

## Ignore IGoR
# mkdir ./data/igor
# mkdir ./data/igor/alpha
# mkdir ./data/igor/beta
# docker run --rm -v /$(pwd):/tmp -w /tmp igor igor -threads 10 -species human -chain alpha -read_seqs ./data/tcrsim/$sim_meth/alpha/$fastQin -align

### this works but MiTCR will not output the fully assembled TCR seq :(
mkdir ./data/mitcr
mkdir ./data/mitcr/alpha
mkdir ./data/mitcr/beta
docker run --rm -v /$(pwd):/tmp -w /tmp mitcr java -Xmx10g -jar ../mitcr.jar -t 16 -pset flex -ec 1 -pcrec smd -species hs -gene TRA -level 3 ./data/tcrsim/$sim_meth/alpha/$fastQin ./data/mitcr/alpha/${fastQin/.fq/.out}
docker run --rm -v /$(pwd):/tmp -w /tmp mitcr java -Xmx10g -jar ../mitcr.jar -t 16 -pset flex -ec 1 -pcrec smd -species hs -gene TRB -level 3 ./data/tcrsim/$sim_meth/beta/$fastQin ./data/mitcr/beta/${fastQin/.fq/.out}

### mixcr script works and gives imputed VDJ transcript sequence.
mkdir ./data/mixcr
mkdir ./data/mixcr/alpha
mkdir ./data/mixcr/beta
docker run --rm -v /$(pwd):/tmp -w /tmp mixcr ./scripts/mixcr.sh $inDir $fastQin $chain $outDir $threads
docker run --rm -v /$(pwd):/tmp -w /tmp mixcr ./scripts/mixcr.sh $inDir $fastQin $chain $outDir $threads 


### Need to write a clone aggregation script since this still outputs multiple germlines for each clone
### Need method to impute or reconstruct complete germline sequences
mkdir ./data/immcant
mkdir ./data/immcant/alpha
mkdir ./data/immcant/beta

docker run --rm -v /$(pwd):/tmp -w /tmp immcantation/suite:4.3.0 ./scripts/immanctation.sh $inDir $fastQin $chain $outDir $threads

#docker run --rm -v /$(pwd):/tmp -w /tmp immcantation/suite:4.3.0 ParseHeaders.py add --fasta -s ./data/tcrsim/igor/alpha/donor_1_1.fq -o ./immcant_tmp/donor_1_1.fa -f BARCODE -u donor_1_1
#docker run --rm -v /$(pwd):/tmp -w /tmp immcantation/suite:4.3.0 AlignSets.py muscle --nproc 10 --bf BARCODE -s ./immcant_tmp/donor_1_1.fa --outdir ./immcant_tmp/ --outname donor_1_1
#docker run --rm -v /$(pwd):/tmp -w /tmp immcantation/suite:4.3.0 BuildConsensus.py --nproc 10 -s ./immcant_tmp/donor_1_1_align-pass.fasta --outdir ./immcant_tmp --outname donor_1_1 --bf BARCODE -n 1
#docker run --rm -v /$(pwd):/tmp -w /tmp immcantation/suite:4.3.0 AssignGenes.py igblast --nproc 10 -b /usr/local/share/igblast --organism human --loci tr --format blast -s ./immcant_tmp/donor_1_1.fa --outdir ./immcant_tmp --outname donor_1_1 

#docker run --rm -v /$(pwd):/tmp -w /tmp immcantation/suite:4.3.0 MakeDb.py igblast -r /usr/local/share/germlines/imgt/human/vdj/imgt_human_TRAV.fasta  /usr/local/share/germlines/imgt/human/vdj/imgt_human_TRAJ.fasta -s ./immcant_tmp/donor_1_1.fa -i ./immcant_tmp/donor_1_1_igblast.fmt7 --outdir ./immcant_tmp --outname donor_1_1
## might need to reduce --maxmiss in production/real-life data
#docker run --rm -v /$(pwd):/tmp -w /tmp immcantation/suite:4.3.0 DefineClones.py --nproc 10 --mode gene --model hh_s1f --norm mut --maxmiss 10 --dist 1 -d ./immcant_tmp/donor_1_1_db-pass.tsv --outdir ./immcant_tmp --outname donor_1_1
## this produces multiple entries per clone and will need to be reduced/aggregated by barcode... especially if assuming single cell.
#docker run --rm -v /$(pwd):/tmp -w /tmp immcantation/suite:4.3.0 CreateGermlines.py -r /usr/local/share/germlines/imgt/human/vdj/imgt_human_TRAV.fasta  /usr/local/share/germlines/imgt/human/vdj/imgt_human_TRAJ.fasta -g full --cloned -d ./immcant_tmp/donor_1_1_clone-pass.tsv --outdir ./immcant_tmp --outname donor_1_1
#awk '{print $(NF-4), "\t",$(NF-3), "\t",$(NF-2), "\t",$(NF-1), "\t",$NF}' ./immcant_tmp/donor_1_1_germ-pass.tsv
