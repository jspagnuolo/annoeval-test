library(Biostrings, quietly = T)
library(parallel, quietly = T)
library(stringr, quietly = T)
source("./scripts/extractKmers.R")

### call: Rscript igor_kmerise.R 75 0.1 10 ./data/tcrsim/igor
### extracts 75-mers with sample rate of 10% of kmer pool for each TCR, uses 10 cores for parApply step

args = commandArgs(trailingOnly=TRUE)

## Alpha Chain
igorfiles <- list.files(path=paste(args[4],"/alpha",sep="") all.files = T, full.names = T, pattern = "*.tsv",
                        recursive = F, include.dirs = F, no.. = T)
igor <- list()
ds <- as.numeric(args[2])
outdir <- paste(args[4],"/alpha",sep="")
cl <- makeCluster(getOption("cl.cores",as.integer(args[3])))
clusterExport(cl, c("ds","outdir"), envir = environment())
for(i in 1:length(igorfiles))
{
  donor <- read.table(igorfiles[i], sep="\t", header = T)
  donor$tcr_id <- paste(paste("donor",i, sep="_"), rownames(donor), sep = "_")
  
  ### apple extractKmer over rows
  parApply(cl, donor, 1, FUN=function(x)
    {
      source("./scripts/extractKmers.R"); ## needs to be sourced for each thread :/
      extractKmer(x["nt_sequence"], 75, x["tcr_id"],writeFq = TRUE, path.out=outdir, downsample=ds)
    })
  igor[[paste("donor",i, sep="_")]] <- donor
}
saveRDS(igor, file=paste(args[4], "/igor_alpha.RDS", sep=""))
stopCluster(cl)

## Beta chain
igorfiles <- list.files(path=paste(args[4],"/beta",sep=""), all.files = T, full.names = T, pattern = "*.tsv",
                        recursive = F, include.dirs = F, no.. = T)
igor <- list()
outdir <- paste(args[4],"/beta",sep="")
cl <- makeCluster(getOption("cl.cores",as.integer(args[3])))
clusterExport(cl, c("ds"), envir = environment())
for(i in 1:length(igorfiles))
{
  donor <- read.table(igorfiles[i], sep="\t", header = T)
  donor$tcr_id <- paste(paste("donor",i, sep="_"), rownames(donor), sep = "_")
  
  ### apply extractKmer over rows
  parApply(cl, donor, 1, FUN=function(x)
  { 
    source("./scripts/extractKmers.R"); ## needs to be sourced for each thread :/
    extractKmer(x["nt_sequence"], 75, x["tcr_id"], writeFq = TRUE, path.out=outdir, downsample=ds)
  })
  igor[[paste("donor",i, sep="_")]] <- donor
}
saveRDS(igor, file=paste(args[4], "/igor_beta.RDS", sep=""))
stopCluster(cl)