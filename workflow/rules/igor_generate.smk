rule igor_generate:
    output:

    params:
        nTCR = config["nTCR"],
        chain = TRA
    container:
        "docker://immunoprobs" ##check this ### immunoprobs is a wrapper for igor with some handy utils
    message: "Generating config[nTCR] simulated TCRs over config[replicates] replicates using {threads} threads"
    shell:
		"""
		if [[{params.chain} == "TRA"]]; then
		    immuno-probs -threads {threads} -out-name {output.sample} -set-wd config[TCRalpha] generate -model human-t-alpha -n-gen {params.nTCR}
		elif [[{params.chain} == "TRB"]]; then
		    immuno-probs -threads {threads} -out-name {output.sample} -set-wd config[TCRbeta] generate -model human-t-beta -n-gen {params.nTCR}
		else
		    echo "incorrect chain parameter"
		fi
		"""