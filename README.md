# TCR Annotation Method Evaluation


## Purpose

Contains scripts and report generation for evaluating and benchmarking VDJ-call accuracy of various TCR annotation packages hosted on the Ridgeline/Matterhorn docker server.

### Packages Evaluated

 - (MiGMAP)[https://github.com/mikessh/migmap]
 - (Immcantation)[https://bitbucket.org/kleinstein/immcantation/src/master/]
 - (TRUST4)[https://github.com/liulab-dfci/TRUST4]
 - (IGoR)[https://github.com/qmarcou/IGoR]
 - (MiTCR)[https://github.com/milaboratory/mitcr]
 - (MiXCR)[https://github.com/milaboratory/mixcr]
 
## Methods

Synthetic TCR sequence data for both alpha and beta chains will be produced using both IGoR (via the immuno-probs utility pacakge) and the R pacakge (immuneSIM)[https://cran.r-project.org/web/packages/immuneSIM/].

While both of these packages do not produce sequence data directly, each output synthetic TCR sequence will be printed to individual fastq files to simulate single cell sequence data.

## Running instructions

clone repo and run main scripts in main folder of the repo.

```
git clone https://bitbucket.com/jspagnuolo/annoeval.git annoeval
cd annoeval

chmod -R +x /scripts
```

To generate synthetic TCRs using IGoR (with simulated sequencing errors) and immuneSIM:
ImmuneSIM is *very* slow so only 1000 sequences are generated per "donor".

```
sh ./scripts/docker_gen.sh
```

output tables, RDS and fasta files will be located under ./data/tcrsim/

To start annotation scripts:
```
sh ./scripts/docker_anno.sh
```
